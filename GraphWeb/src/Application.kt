package ru.appmat

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.html.*
import kotlinx.html.*
import kotlinx.css.*
import io.ktor.locations.*
import io.ktor.sessions.*
import io.ktor.features.*
import io.ktor.webjars.*
import java.time.*
import io.ktor.auth.*
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import kotlinx.css.Float
import kotlinx.css.properties.TextDecoration

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(Locations) {
    }

    install(Sessions) {
        cookie<MySession>("MY_SESSION") {
            cookie.extensions["SameSite"] = "lax"
        }
    }

    install(Webjars) {
        path = "/webjars" //defaults to /webjars
        zone = ZoneId.systemDefault() //defaults to ZoneId.systemDefault()
    }

    install(Authentication) {
        basic("myBasicAuth") {
            realm = "Ktor Server"
            validate { if (it.name == "test" && it.password == "password") UserIdPrincipal(it.name) else null }
        }
    }

    install(ContentNegotiation) {
    }

    val client = HttpClient(Apache) {
        install(JsonFeature){
            serializer= KotlinxSerializer()
        }
    }

    routing {
        get<Graph.Home>{
            call.respondHtml{
                head{
                    styleLink(url="/styles.css")
                }
                body{
                    div(classes="topnav"){
                        a(classes="active"){
                            +"Главная"
                        }
                        input {
                            placeholder="Поиск"
                        }
                        a(classes="auth"){
                            +"Войти"
                        }
                    }
                    p {
                        a("/graphcourse/courses") {
                            +"Курсы"
                        }
                    }
                    p {
                        a("/graphcourse/specialties") {
                            +"Специальности"
                        }
                    }
                    p{
                        a("/graphcourse/subjects") {
                            +"Темы"
                        }
                    }

                }
            }
        }

        get<Graph.Courses>{
            val courses=client.get<List<Course>>("http://localhost:4040/api/course")
            call.respondHtml {
                head{
                    styleLink(url="/styles.css")
                }
                body {
                    div(classes="topnav"){
                        a("/graphcourse/home"){
                            +"Главная"
                        }
                        input {
                            placeholder="Поиск"
                        }
                        a(classes="auth"){
                            +"Войти"
                        }
                        a(classes="title"){
                            +"Список всех курсов в базе данных"
                        }
                    }
                    div(classes="niceList")
                    {
                        ul {
                            courses.forEach {
                                li { +it.name }
                            }
                        }
                    }

                }
            }
        }

        get<Graph.Specialties>{
            val specialties=client.get<List<Specialty>>("http://localhost:4040/api/specialty")
            call.respondHtml {
                head{
                    styleLink(url="/styles.css")
                }
                body {
                    div(classes="topnav"){
                        a("/graphcourse/home") {
                            +"Главная"
                        }
                        a(classes="title"){
                            +"Список всех специальностей в базе данных"
                        }
                        input {
                            placeholder="Поиск"
                        }
                        a(classes="auth"){
                            +"Войти"
                        }
                    }
                    div(classes="niceList")
                    {
                        ul(classes = "active") {
                            specialties.forEach {
                                //li { a("/graphcourse/specialties/${it.id}") { +it.name } }
                                li {

                                    onClick = "location.href='/graphcourse/specialties/${it.id}'";
                                    +it.name
                                }
                            }
                        }
                    }
                }
                
            }
        }

        get<Graph.Subjects>{
            val subjects=client.get<List<Subject>>("http://localhost:4040/api/subject")
            call.respondHtml {
                head{
                    styleLink(url="/styles.css")
                }
                body {
                    div(classes="topnav"){
                        a("/graphcourse/home"){
                            +"Главная"
                        }
                        input {
                            placeholder="Поиск"
                        }
                        a(classes="auth"){
                            +"Войти"
                        }
                        a(classes="title"){
                            +"Список всех тем в базе данных"
                        }
                    }
                    div(classes="niceList")
                    {
                        ul {
                            subjects.forEach {
                                li { +it.name }
                            }
                        }
                    }

                }

            }
        }

        get<Graph.Specialties.CoursesId>{
            val id=it.id
            val specialty=client.get<Specialty>("http://localhost:4040/api/specialty/${id}")
            val courses=client.get<List<Course>>("http://localhost:4040/api/specialty/${id}/courses")
            call.respondHtml {
                head{
                    styleLink(url="/styles.css")
                }
                body {
                    div(classes="topnav"){
                        a("/graphcourse/home"){
                            +"Главная"
                        }
                        input {
                            placeholder="Поиск"
                        }
                        a(classes="auth"){
                            +"Войти"
                        }
                        a(classes="title"){
                            +"Специальность: ${specialty.name}"
                        }
                    }
                    div(classes="niceList")
                    {
                        ul {
                            courses.forEach {
                                li { +it.name }
                            }
                        }
                    }

                }
            }
        }

        get("/styles.css") {
            call.respondCss {
                //====================================[Верхняя панель навигации]====================================
                rule(".topnav") {
                    backgroundColor = Color("#333")
                    overflow = Overflow.hidden
                    marginBottom=3.em
                }

                rule(".topnav a") {
                    float = Float.left
                    color= Color("#f2f2f2")
                    textAlign=TextAlign.center
                    padding="4px 16px"
                    textDecoration= TextDecoration.none
                    fontSize=1.em
                    margin="0.5em"
                    borderRadius=0.625.em
                }

                rule(".topnav a:hover"){
                    backgroundColor=Color("#bfbfbf")
                    color= Color.black
                }
                rule(".topnav a.active"){
                    backgroundColor = Color("#bf3b3f")
                    color= Color.white
                    margin="0.5em"
                }
                rule(".topnav input"){
                    float=Float.right;
                    padding="4px"
                    margin="0.5em"
                    marginRight=17.em
                    fontSize=1.em
                    borderRadius=0.625.em
                    border="none"
                }
                rule(".topnav a.auth"){
                    backgroundColor = Color("#595858")
                    color= Color.white
                    float=Float.right;
                    padding="4px 30px"
                    margin="0.5em"
                    marginRight=-25.em
                    fontSize=1.em
                    borderRadius=0.625.em
                    //border="none"
                }
                rule(".topnav a.auth:hover"){
                    backgroundColor=Color("#bf3b3f")
                    marginLeft=0.1.em
                }
                rule(".topnav a.title"){
                    color= Color.white
                    marginLeft=25.em
                }
                rule(".topnav a.title:hover"){
                    backgroundColor = Color("#333")
                }
                //========================================================================

                body {
                    fontFamily = "Avantgarde, TeX Gyre Adventor, URW Gothic L,sans-serif"
                    backgroundColor = Color.white
                    margin="0"
                }
                p {
                    fontSize = 1.5.em
                    marginLeft=2.em
                    //margin="0.5em"
                }
                a{
                    textDecoration= TextDecoration.none
                    color= Color.black
                }
                //========================================================================
                rule(".niceList ul"){
                    marginLeft=0.4.em
                    marginRight=0.4.em
                }
                rule(".niceList ul li"){
                    display=Display.inlineBlock
                    fontSize = 1.1.em
                    backgroundColor = Color("#ddd")
                    marginRight=0.4.em
                    marginBottom=0.6.em
                    padding="30px 50px"
                }
                rule(".niceList ul.active li:hover"){
                    backgroundColor = Color("#bf3b3f")
                    color= Color.white
                }

                h1{
                    textAlign=TextAlign.center
                    margin="0.5em"
                }


            }
        }

        get("/session/increment") {
            val session = call.sessions.get<MySession>() ?: MySession()
            call.sessions.set(session.copy(count = session.count + 1))
            call.respondText("Counter is ${session.count}. Refresh to increment.")
        }

        install(StatusPages) {
            exception<AuthenticationException> { cause ->
                call.respond(HttpStatusCode.Unauthorized)
            }
            exception<AuthorizationException> { cause ->
                call.respond(HttpStatusCode.Forbidden)
            }

        }

        get("/webjars") {
            call.respondText("<script src='/webjars/jquery/jquery.js'></script>", ContentType.Text.Html)
        }

        authenticate("myBasicAuth") {
            get("/protected/route/basic") {
                val principal = call.principal<UserIdPrincipal>()!!
                call.respondText("Hello ${principal.name}")
            }
        }
    }
}

data class MySession(val count: Int = 0)

class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()

fun FlowOrMetaDataContent.styleCss(builder: CSSBuilder.() -> Unit) {
    style(type = ContentType.Text.CSS.toString()) {
        +CSSBuilder().apply(builder).toString()
    }
}

fun CommonAttributeGroupFacade.style(builder: CSSBuilder.() -> Unit) {
    this.style = CSSBuilder().apply(builder).toString().trim()
}

suspend inline fun ApplicationCall.respondCss(builder: CSSBuilder.() -> Unit) {
    this.respondText(CSSBuilder().apply(builder).toString(), ContentType.Text.CSS)
}
