package ru.appmat
import kotlinx.serialization.Serializable

@Serializable
data class Specialty(
        val id: Long,
        val name: String
) {

}