package ru.appmat.specialty

import io.ktor.application.*
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.transactions.transaction
import ru.appmat.API

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads

fun Application.specialtyModule() {
    val specialtyService = SpecialtyDAO(SpecialtyTable, CourseToSpecialtyTable)
    routing {
        specialty(specialtyService)
    }
}

internal fun Routing.specialty(specialtyService: SpecialtyDAO) {
    get<API.Specialties> {
        val specialtyList = transaction {
            specialtyService.list()
        }
        call.respond(specialtyList)
    }
    post<API.Specialties.Specialty> {
        val specialty = call.receive<Specialty>()
        transaction {
            specialtyService.persist(specialty)
        }
        call.respond(Created)
    }
    get<API.Specialties.Specialty> {
        val specialty = transaction {
            specialtyService.load(it.id)
        }
        call.respond(specialty)
    }
    put<API.Specialties.Specialty> {
        val specialty = call.receive<Specialty>()
        transaction {
            specialtyService.update(specialty)
        }
        call.respond(OK)
    }
    delete<API.Specialties.Specialty> {
        transaction {
            specialtyService.delete(it.id)
        }
        call.respond(OK)
    }

    //
    get<API.Courses.Course.Specialties>{
        val specialties= transaction {
            specialtyService.loadByCourse(it.parent.id)
        }
        call.respond(specialties)
    }

    put<API.Specialties.Specialty.Courses>{
        val courseIds=call.receive<Set<Long>>()
        transaction {
            specialtyService.updateCourses(it.parent.id,courseIds)
        }
        call.respond(OK)
    }
}