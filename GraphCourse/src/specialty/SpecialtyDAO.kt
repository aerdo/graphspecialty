package ru.appmat.specialty

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder

internal class SpecialtyDAO(
        private val specialtyTable: SpecialtyTable,
        private val courseToSpecialtyTable: CourseToSpecialtyTable
) {
    private fun ResultRow.toSpecialty()= Specialty(
            id=this[SpecialtyTable.id].value,
            name=this[SpecialtyTable.name]
    )

    private fun UpdateBuilder<*>.fromSpecialty(specialty: Specialty){
        this[SpecialtyTable.name] = specialty.name
    }

    fun load(id: Long) =
            specialtyTable.select {
                SpecialtyTable.id eq id
            }.single().toSpecialty()

    fun list() =
            specialtyTable.selectAll()
                    .mapNotNull {
                        it.toSpecialty()
                    }

    fun persist(specialty: Specialty) =
            specialtyTable.insertAndGetId {
                it[id] = specialty.id
                it.fromSpecialty(specialty)
            }.value

    fun update(specialty: Specialty) =
            specialtyTable.update({
                SpecialtyTable.id eq specialty.id
            }) {
                it.fromSpecialty(specialty)
            }

    fun delete(id: Long) =
            specialtyTable.deleteWhere {
                SpecialtyTable.id eq id
            }

    // работа со связями
    fun loadByCourse(courseId: Long) =
            (courseToSpecialtyTable innerJoin specialtyTable)
                    .slice(specialtyTable.columns)
                    .select{
                        CourseToSpecialtyTable.courseId eq courseId
                    }.mapNotNull {
                        it.toSpecialty()
                    }

    fun updateCourses(id: Long, courseIds: Set<Long>){
        val currentCourseIds=courseToSpecialtyTable.slice(courseToSpecialtyTable.courseId)
                .select{
                    courseToSpecialtyTable.specialtyId eq id}.mapNotNull {
                    it[courseToSpecialtyTable.courseId]
                }
        courseToSpecialtyTable.deleteWhere {
            (specialtyTable.id eq id) and (courseToSpecialtyTable.courseId inList currentCourseIds.minus(courseIds))
        }

        courseToSpecialtyTable.batchInsert(courseIds.minus(currentCourseIds)){courseId->
            this[courseToSpecialtyTable.specialtyId]=id
            this[courseToSpecialtyTable.courseId]=courseId
        }
    }

}
