package ru.appmat.specialty

data class Specialty(
        val id: Long,
        val name: String
) {

}