package ru.appmat.specialty

import org.jetbrains.exposed.sql.Table

internal object CourseToSpecialtyTable : Table("course_to_specialty") {
    val courseId = long("course_id")
    val specialtyId = reference("specialty_id", SpecialtyTable)
}