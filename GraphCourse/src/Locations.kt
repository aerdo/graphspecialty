package ru.appmat

import io.ktor.locations.*

@Location("/api")
class API {
    @Location("/course")
    class Courses(val api: API) {
        @Location("/{id}")
        data class Course(val id: Long, val parent: Courses) {
            @Location("/specialties")
            data class Specialties(val parent: Course)
        }
    }

    @Location("/specialty")
    class Specialties(val api: API) {
        @Location("/{id}")
        data class Specialty(val id: Long, val parent: Specialties) {
            @Location("/courses")
            data class Courses(val parent: Specialty)
        }
    }

    @Location("/subject")
    class Subjects(val api: API) {
        @Location("/{id}")
        data class Subject(val id: Long, val parent: Subjects) {
        }
    }
}
