package ru.appmat.course

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.Column

internal object CourseTable : LongIdTable("course") {
    val name: Column<String> = varchar("name", 150)
}