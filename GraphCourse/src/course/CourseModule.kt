package ru.appmat.course

import io.ktor.application.*
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.transactions.transaction
import ru.appmat.API

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.courseModule() {
    val courseService = CourseDAO(CourseTable, SpecialtyToCourseTable)
    routing {
        course(courseService)
    }
}

internal fun Routing.course(courseService: CourseDAO) {
    get<API.Courses> {
        val courseList = transaction {
            courseService.list()
        }
        call.respond(courseList)
    }
    post<API.Courses.Course> {
        val course = call.receive<Course>()
        transaction {
            courseService.persist(course)
        }
        call.respond(Created)
    }
    get<API.Courses.Course> {
        val course = transaction {
            courseService.load(it.id)
        }
        call.respond(course)
    }
    put<API.Courses.Course> {
        val course = call.receive<Course>()
        transaction {
            courseService.update(course)
        }
        call.respond(OK)
    }
    delete<API.Courses.Course> {
        transaction {
            courseService.delete(it.id)
        }
        call.respond(OK)
    }

    // работа со связями
    get<API.Specialties.Specialty.Courses> {
        val courses = transaction {
            courseService.loadBySpecialty(it.parent.id)
        }
        call.respond(courses)
    }

    put<API.Courses.Course.Specialties> {
        val specialtyIds = call.receive<Set<Long>>()
        transaction {
            courseService.updateSpecialties(it.parent.id, specialtyIds)
        }
        call.respond(OK)
    }
}