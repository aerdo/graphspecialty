package ru.appmat.course

import org.jetbrains.exposed.sql.Table

internal object SpecialtyToCourseTable : Table("specialty_to_course") {
    val specialtyId = long("specialty_id")
    val courseId = reference("course_id", CourseTable)
}