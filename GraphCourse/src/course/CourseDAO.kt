package ru.appmat.course

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder

internal class CourseDAO(
        private val courseTable: CourseTable,
        private val specialtyToCourseTable: SpecialtyToCourseTable
) {
    private fun ResultRow.toCourse() = Course(
            id = this[CourseTable.id].value,
            name = this[CourseTable.name]
    )

    private fun UpdateBuilder<*>.fromCourse(course: Course) {
        this[CourseTable.name] = course.name
    }

    fun load(id: Long) =
            courseTable.select {
                CourseTable.id eq id
            }.single().toCourse()

    fun list() =
            courseTable.selectAll()
                    .mapNotNull {
                        it.toCourse()
                    }

    fun persist(course: Course) =
            courseTable.insertAndGetId {
                it[id] = course.id
                it.fromCourse(course)
            }.value

    fun update(course: Course) =
            courseTable.update({
                CourseTable.id eq course.id
            }) {
                it.fromCourse(course)
            }

    fun delete(id: Long) =
            courseTable.deleteWhere {
                CourseTable.id eq id
            }

    // работа со связями
    fun loadBySpecialty(specialtyId: Long) =
            (specialtyToCourseTable innerJoin courseTable)
                    .slice(courseTable.columns)
                    .select {
                        SpecialtyToCourseTable.specialtyId eq specialtyId
                    }.mapNotNull {
                        it.toCourse()
                    }

    fun updateSpecialties(id: Long, specialtyIds: Set<Long>) {
        val currentSpecialtyIds = specialtyToCourseTable.slice(specialtyToCourseTable.specialtyId)
                .select { specialtyToCourseTable.courseId eq id }.mapNotNull {
                    it[specialtyToCourseTable.specialtyId]
                }
        specialtyToCourseTable.deleteWhere {
            (courseTable.id eq id) and (specialtyToCourseTable.specialtyId inList currentSpecialtyIds.minus(specialtyIds))
        }

        specialtyToCourseTable.batchInsert(specialtyIds.minus(currentSpecialtyIds)) { specialtyId ->
            this[specialtyToCourseTable.courseId] = id
            this[specialtyToCourseTable.specialtyId] = specialtyId
        }
    }

}