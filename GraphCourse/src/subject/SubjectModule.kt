package ru.appmat.subject

import io.ktor.application.*
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.transactions.transaction
import ru.appmat.API

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads

fun Application.subjectModule() {
    val subjectService = SubjectDAO(SubjectTable)
    routing {
        subject(subjectService)
    }
}

internal fun Routing.subject(subjectService: SubjectDAO) {
    get<API.Subjects> {
        val subjectList = transaction {
            subjectService.list()
        }
        call.respond(subjectList)
    }
    post<API.Subjects.Subject> {
        val subject = call.receive<Subject>()
        transaction {
            subjectService.persist(subject)
        }
        call.respond(Created)
    }
    get<API.Subjects.Subject> {
        val subject = transaction {
            subjectService.load(it.id)
        }
        call.respond(subject)
    }
    put<API.Subjects.Subject> {
        val subject = call.receive<Subject>()
        transaction {
            subjectService.update(subject)
        }
        call.respond(OK)
    }
    delete<API.Subjects.Subject> {
        transaction {
            subjectService.delete(it.id)
        }
        call.respond(OK)
    }
}