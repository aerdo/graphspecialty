package ru.appmat.subject

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder

internal class SubjectDAO(private val subjectTable: SubjectTable) {
    private fun ResultRow.toSubject()=Subject(
            id = this[SubjectTable.id].value,
            name = this[SubjectTable.name],
            description = this[SubjectTable.description],
            /*spec = this[SubjectTable.spec],
            cour = this[SubjectTable.cour],
            num = this[SubjectTable.num]*/
    )

    private fun UpdateBuilder<*>.fromSubject(subject: Subject){
        this[SubjectTable.name] = subject.name
        this[SubjectTable.description] = subject.description
        /*this[SubjectTable.spec] = subject.spec
        this[SubjectTable.cour] = subject.cour
        this[SubjectTable.num] = subject.num*/
    }

    fun load(id: Long) =
            subjectTable.select {
                SubjectTable.id eq id
            }.single().toSubject()

    fun list() =
            subjectTable.selectAll()
                    .mapNotNull {
                        it.toSubject()
                    }

    fun persist(subject: Subject) =
            subjectTable.insertAndGetId {
                it[SubjectTable.id] = subject.id
                it.fromSubject(subject)
            }.value

    fun update(course: Subject) =
            subjectTable.update({
                SubjectTable.id eq course.id
            }) {
                it.fromSubject(course)
            }

    fun delete(id: Long) =
            subjectTable.deleteWhere {
                SubjectTable.id eq id
            }
}