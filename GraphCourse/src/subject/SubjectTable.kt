package ru.appmat.subject

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.Column

internal object SubjectTable : LongIdTable("subject"){
    val name: Column<String> = varchar("name", 150)
    val description: Column<String> = varchar("description", 350)
    /*val spec: Column<Long> = long("spec")
    val cour: Column<Long> = long("cour")
    val num: Column<Long> = long("num")*/
}